import React, {useState} from 'react';
import { useEffect } from 'react';

const Counter = () => {
    const [count, setCount] = useState(0);
    const [count2, setCount2] = useState(0);

    const countUp = () => {
        setCount(prevState => prevState + 1)
    }

    const countDown = () => {
        setCount(prevState => prevState - 1)
    }

    const count2Up = () => {
        setCount2(prevState => prevState + 1)
    }

    const count2Down = () => {
        setCount2(prevState => prevState - 1)
    }

    useEffect( () => {
        console.log("Current count is ...", count);
    }, [count])

    return (
        <div>
            <p>
                現在のカウント数: {count}
            </p>
            <button onClick={countUp}>up</button>
            <button onClick={countDown}>down</button>

            <p>
                現在のカウント2数: {count2}
            </p>
            <button onClick={count2Up}>up2</button>
            <button onClick={count2Down}>down2</button>
        </div>
    );
};

export default Counter;