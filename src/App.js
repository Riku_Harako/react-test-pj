// import logo from './logo.svg';
// import './App.css';
import React, {useState} from 'react';
import { useEffect } from "react";
import {Article, TextInput, Counter, ToggleButton} from "./components";

function App() {
  const [id, setId] = useState('deatiger')
  const [name, setName] = useState('')
  const ids = ["deatiger", "aws", "google", "facebook"]
  const getRandomId = () => {
    const _id = ids[Math.floor(Math.random() * ids.length)]
    setId(_id)
  }

  useEffect( () => {
    fetch(`https://api.github.com/users/${id}`)
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setName(data.name)
      })
      .catch(error => {
        console.log(error)
      })
  }, [id])

  return (
    <div>
        {/* <Article
          title = {'がんばろう！'}
          content = {'大変だ～'}
        /> */}
        {/* <TextInput /> */}
        {/* <Counter /> */}
        {/* <ToggleButton/> */}
        <p>{id}の、Github上の名前は{name}です</p>
        <button onClick={getRandomId}>IDを変更</button>
    </div>
  );
}

export default App;
